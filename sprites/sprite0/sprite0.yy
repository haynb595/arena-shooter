{
    "id": "52cc75e9-fc90-4387-a4a1-1df693d5213f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite0",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 54,
    "bbox_left": 9,
    "bbox_right": 57,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "70125fe5-5b99-4cfc-a35d-39aea8233a86",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "52cc75e9-fc90-4387-a4a1-1df693d5213f",
            "compositeImage": {
                "id": "7c0f6a9f-acf5-4d64-82c7-84020c00c88b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "70125fe5-5b99-4cfc-a35d-39aea8233a86",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a5533c40-4dc5-46c7-bffc-d363e1f78d46",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "70125fe5-5b99-4cfc-a35d-39aea8233a86",
                    "LayerId": "c5ebb5d7-b56d-4bff-91e5-3c92cd7bf16c"
                }
            ]
        },
        {
            "id": "70a658f4-fd15-4ce8-a9af-2ea9d5f60893",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "52cc75e9-fc90-4387-a4a1-1df693d5213f",
            "compositeImage": {
                "id": "8c8eb2ff-1eb7-4663-92f4-fe9a35fbf0b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "70a658f4-fd15-4ce8-a9af-2ea9d5f60893",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "61ef18d1-ba14-4899-982a-9a1957bf06b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "70a658f4-fd15-4ce8-a9af-2ea9d5f60893",
                    "LayerId": "c5ebb5d7-b56d-4bff-91e5-3c92cd7bf16c"
                }
            ]
        },
        {
            "id": "580eba69-2bbd-4694-b76b-daa9a6a1a606",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "52cc75e9-fc90-4387-a4a1-1df693d5213f",
            "compositeImage": {
                "id": "b6923a0e-e211-40b5-8be1-b720c3fdf08a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "580eba69-2bbd-4694-b76b-daa9a6a1a606",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d9b1f29-a06c-4c2c-992f-617cd107017b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "580eba69-2bbd-4694-b76b-daa9a6a1a606",
                    "LayerId": "c5ebb5d7-b56d-4bff-91e5-3c92cd7bf16c"
                }
            ]
        },
        {
            "id": "3ec721cc-1b9c-418a-bb34-edb1edbe6600",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "52cc75e9-fc90-4387-a4a1-1df693d5213f",
            "compositeImage": {
                "id": "9d0da718-bfe2-458c-a4a5-55e4928084b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3ec721cc-1b9c-418a-bb34-edb1edbe6600",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "101327b9-a08c-4bcd-bbef-c2266689174f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ec721cc-1b9c-418a-bb34-edb1edbe6600",
                    "LayerId": "c5ebb5d7-b56d-4bff-91e5-3c92cd7bf16c"
                }
            ]
        },
        {
            "id": "0fabc315-d94a-4b70-a0fa-75b3fbdf2884",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "52cc75e9-fc90-4387-a4a1-1df693d5213f",
            "compositeImage": {
                "id": "aeffc352-5746-4bcb-b118-146e73066285",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0fabc315-d94a-4b70-a0fa-75b3fbdf2884",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0c34a2e-0315-4a68-ab27-88c0f151ab97",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0fabc315-d94a-4b70-a0fa-75b3fbdf2884",
                    "LayerId": "c5ebb5d7-b56d-4bff-91e5-3c92cd7bf16c"
                }
            ]
        },
        {
            "id": "904691cd-5304-4bb6-b77e-e49985a5c7dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "52cc75e9-fc90-4387-a4a1-1df693d5213f",
            "compositeImage": {
                "id": "83193724-f6e2-487c-897a-8fb077694c17",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "904691cd-5304-4bb6-b77e-e49985a5c7dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82c3bb07-4f70-4c1e-b22c-59b681010ecf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "904691cd-5304-4bb6-b77e-e49985a5c7dc",
                    "LayerId": "c5ebb5d7-b56d-4bff-91e5-3c92cd7bf16c"
                }
            ]
        },
        {
            "id": "79cdc350-7247-484b-ac63-2a55b4cb9f36",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "52cc75e9-fc90-4387-a4a1-1df693d5213f",
            "compositeImage": {
                "id": "15aec580-3001-4b4c-9e3c-9a6a091960d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "79cdc350-7247-484b-ac63-2a55b4cb9f36",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7698f5ae-050c-4bd9-8065-b87bdb69aa46",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "79cdc350-7247-484b-ac63-2a55b4cb9f36",
                    "LayerId": "c5ebb5d7-b56d-4bff-91e5-3c92cd7bf16c"
                }
            ]
        },
        {
            "id": "e6572879-a91e-4e35-acf0-72e8634342e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "52cc75e9-fc90-4387-a4a1-1df693d5213f",
            "compositeImage": {
                "id": "0b0ddb9b-81d4-487e-9f12-62444f96b42a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e6572879-a91e-4e35-acf0-72e8634342e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0e9215a-53fe-41b0-9394-5c58a51b6bda",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6572879-a91e-4e35-acf0-72e8634342e8",
                    "LayerId": "c5ebb5d7-b56d-4bff-91e5-3c92cd7bf16c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "c5ebb5d7-b56d-4bff-91e5-3c92cd7bf16c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "52cc75e9-fc90-4387-a4a1-1df693d5213f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}